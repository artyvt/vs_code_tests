package test_connection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CreateEmployee {
    public static void main(String[] args) {

        SessionFactory factory = new Configuration()    
                .configure("hibernate.cfg.xml")
                .addPackage("test_connection")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
            System.out.println("Creating new employee object....");
            Employee employee = new Employee("Arturs", "Vaitilavics", "Arturs.Vaitilavics@gmail.com");
            session.beginTransaction();

            System.out.println("Saving the employee to DB: ");
            session.save(employee);
            session.getTransaction().commit();

            System.out.println("Employee saved to DB");
        } finally {
            factory.close();
        }
    }
}
