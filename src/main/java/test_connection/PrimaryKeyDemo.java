package test_connection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class PrimaryKeyDemo {
    public static void main(String[] args) {

        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addPackage("test_connection")
                .addAnnotatedClass(Employee.class)
                .buildSessionFactory();

        Session session = factory.getCurrentSession();

        try {
//            employeeObjectToDb(session);
//            getEmployeeFromDb(factory);

            session.beginTransaction();


            //Getting data from DB
            List<Employee> employeeList = session.createQuery("from Employee")
                    .list();

            querryEmployees(employeeList);
            System.out.println();


            employeeList = session.createQuery("from Employee employee where employee.surName='Vaitilavics'").list();

            querryEmployees(employeeList);

            System.out.println();
            employeeList =
                    session.createQuery("from Employee employee where " +
                            "employee.surName='Rungalds' or" +
                            " employee.name='Teodors'").list();
            querryEmployees(employeeList);

            System.out.println();
            employeeList = session.createQuery("from Employee employee where" +
                    " employee.email like '%inbox.lv'").list();

            querryEmployees(employeeList);


            //updating data in DB
            int employeeID = 1;

            Employee employee = session.get(Employee.class, employeeID);
            employee.setName("Toto");

            //update data in whole column
            session.createQuery("update Employee set email='local@companyMail.com'").executeUpdate();


            //delete employee from Db
            int employeeID0001 = 3;

            Employee employee00002 = session.get(Employee.class, 3);
//            session.delete(employee00002);

//            alternate approach on deleting data
            session.createQuery("delete from Employee where id=1").executeUpdate();
            session.getTransaction().commit();


        } finally {
            factory.close();
        }
    }

    private static void querryEmployees(List<Employee> employeeList) {
        for (Employee tempEmployee : employeeList) {
            System.out.println(tempEmployee);
        }
    }

    private static void getEmployeeFromDb(SessionFactory factory) {
        Session session;
        session = factory.getCurrentSession();
        session.beginTransaction();
        System.out.println("getting employee data: ");

        Employee employee = session.get(Employee.class, 3001);
        System.out.println("get complete: " + employee);

        session.getTransaction().commit();
    }

    private static void employeeObjectToDb(Session session) {
        System.out.println("Creating new employee object....");
        Employee employee001 = new Employee("Aldis", "Liperksis", "aldis0001@inbox.lv");
        Employee employee002 = new Employee("Zalgis", "Cerls", "gangsta@one.lv");
        Employee employee003 = new Employee("Ingeborga", "Zernegraze", "Zrvaznica@ru.com");


        session.beginTransaction();

        System.out.println("Saving the employee to DB: ");
        session.save(employee001);
        session.save(employee002);
        session.save(employee003);

        session.getTransaction().commit();

        System.out.println("Employee saved to DB");
    }
}
